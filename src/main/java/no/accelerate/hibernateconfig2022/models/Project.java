package no.accelerate.hibernateconfig2022.models;

import org.springframework.transaction.annotation.Transactional;

import javax.persistence.*;

@Entity
@Table(name = "tb_project")
public class Project {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    private String title;

    // Relationships
    @OneToOne(mappedBy = "project")
    private Student student;
}
