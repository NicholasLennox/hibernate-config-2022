package no.accelerate.hibernateconfig2022.models;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name = "tb_professor")
public class Professor {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    private String name;

    // Relationship
    @OneToMany(mappedBy = "professor")
    private Set<Student> students;
}
