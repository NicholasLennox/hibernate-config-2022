package no.accelerate.hibernateconfig2022.models;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name = "tb_student")
public class Student {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    @Column(name = "stud_name", nullable = false, length = 100)
    private String name;

    // Relationships
    @OneToOne
    @JoinColumn(name = "project_id")
    private Project project;

    @ManyToOne
    @JoinColumn(name = "professor_id")
    private Professor professor;

    @ManyToMany
    @JoinTable(
            name = "tb_stud_sub",
            joinColumns = {
                    @JoinColumn(name = "student_id")
            },
            inverseJoinColumns = {
                    @JoinColumn(name = "subject_id")
            }
    )
    private Set<Subject> subjects;
}
