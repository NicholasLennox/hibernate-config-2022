package no.accelerate.hibernateconfig2022.models;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name = "tb_subject")
public class Subject {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    private String code;
    private String description;

    // Relationships
    @ManyToMany(mappedBy = "subjects")
    private Set<Student> students;
}
