package no.accelerate.hibernateconfig2022;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HibernateConfig2022Application {

    public static void main(String[] args) {
        SpringApplication.run(HibernateConfig2022Application.class, args);
    }

}
